package au.com.gharib.jared.lummox.controlproduction.command;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import au.com.gharib.jared.lummox.controlproduction.ControlCore;
import au.com.gharib.jared.lummox.controlproduction.Message;
import au.com.gharib.jared.lummox.controlproduction.PlayerData;
import au.com.gharib.jared.lummox.controlproduction.ControlCore.ControlAction;

/**
 * Command Executor for /controlproduction
 * 
 * @author Jared
 */
public class ControlProduction implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(args.length < 1) 
			return false;
		
		if(args[0].equalsIgnoreCase("reset")) {
			return reset(sender, args);
		} else if(args[0].equalsIgnoreCase("info")) {
			return info(sender, args);
		} else if(args[0].equalsIgnoreCase("infoof")) {
			return infoOf(sender, args);
		}
				
		sender.sendMessage(ControlCore.getMessage(Message.COMMAND_INCORRECT_STRUCTURE));
		return false;
		
	}
	
	private boolean info(CommandSender sender, String[] args) {
		// Players only
		if(!(sender instanceof Player)) {
			sender.sendMessage(ControlCore.getMessage(Message.COMMAND_PLAYERS_ONLY));
			return true;
		}
		Player player   = (Player) sender;
		PlayerData data = new PlayerData(player); 
		
		// Check arguments
		if(args.length != 2) {
			player.sendMessage(ChatColor.RED + ControlCore.getMessage(Message.COMMAND_INCORRECT_STRUCTURE));
			player.sendMessage("/controlproduction info action");
			return true;
		}
		
		// Check player is in a group
		if(!data.isInGroup()) {
			player.sendMessage(ChatColor.GREEN +  ControlCore.getMessage(Message.INFO_NOT_IN_GROUP));
			return true;
		}
		
		// Get action
		ControlAction action;
		try {
			action = ControlAction.valueOf(args[1].toUpperCase());
		} catch (IllegalArgumentException ex) {
			player.sendMessage(ChatColor.RED + ControlCore.getMessage(Message.COMMAND_INVALID_ACTION));
			return true;
		}
		
		// Check player has limits for action
		if(!PlayerData.limitExists(action, null, data.getGroup())) {
			player.sendMessage(ChatColor.GREEN + ControlCore.getMessage(Message.INFO_NO_LIMIT));
			return true;
		}
		
		// Show limits
		Object[] limitsForAction = ControlCore.getInstance()
												.getConfig()
												.getConfigurationSection("groups")
												.getConfigurationSection(data.getGroup())
												.getConfigurationSection(action.toString())
												.getKeys(false)
												.toArray();
		
		player.sendMessage(ChatColor.YELLOW + ControlCore.getMessage(Message.INFO_HEADER)
				.replace("[ACTION]", action.toString().toLowerCase()));
		
		for(Object limitKey : limitsForAction) {
			Material block  = Material.getMaterial(Integer.parseInt((String)limitKey));
			int limitActual = data.getLimit(action, block);
			int playerLimit = limitActual - data.getDone(action, block);
			
			player.sendMessage(ChatColor.YELLOW + ControlCore.getMessage(Message.INFO_LIST)
					.replace("[ACTION]", action.toString().toLowerCase())
					.replace("[LIMIT]", playerLimit + "")
					.replace("[ITEM]", block.toString()));
		}
		
		
		 
		
		return true;
	}
	
	private boolean reset(CommandSender s, String[] args) {
		// Check player permissions
		if(s instanceof Player && !((Player) s).hasPermission("controlproduction.reset")) {
			s.sendMessage(ChatColor.RED + ControlCore.getMessage(Message.COMMAND_NO_PERMISSION));
			return true;
		}
		
		String player;
		ControlAction action;
		Material block;
		
		try {
			player = args[1];
		} catch (Exception ex) {
			player = null;
		}
		
		try {
			action = Enum.valueOf(ControlAction.class, args[2].toUpperCase());
		} catch (Exception ex) {
			action = null;
		}
		
		try {
			block = Material.getMaterial(Integer.parseInt(args[3]));
		} catch (Exception ex) {
			block = null;
		}
		
		
		
		
		switch(args.length) {
			case 1:
				PlayerData.reset(null, null, null);
				s.sendMessage(ControlCore.getMessage(Message.LIMITS_RESET_ALL));
				return true;
			case 2:
				PlayerData.reset(player, null, null);
				s.sendMessage(ControlCore.getMessage(Message.LIMITS_RESET_PLAYER).replace("[PLAYER]", player));
				return true;
			case 3:
				if(action == null) {
					s.sendMessage(ControlCore.getMessage(Message.COMMAND_INVALID_ACTION));
					return true;
				}
				
				PlayerData.reset(player, action, null);
				s.sendMessage(ControlCore.getMessage(Message.LIMITS_RESET_PLAYER).replace("[PLAYER]", player));
				return true;
			case 4:
				if(action == null) {
					s.sendMessage(ControlCore.getMessage(Message.COMMAND_INVALID_ACTION));
					return true;
				}
				if(block == null) {
					s.sendMessage(ControlCore.getMessage(Message.COMMAND_NUMBER_FORMAT_EXCEPTION));
					return true;
				}
				
				PlayerData.reset(player, action, block);
				s.sendMessage(ControlCore.getMessage(Message.LIMITS_RESET_PLAYER).replace("[PLAYER]", player));
				return true;
				
			default:
				s.sendMessage(ControlCore.getMessage(Message.COMMAND_INCORRECT_STRUCTURE));
				s.sendMessage("/controlproduction reset [player] [action] [blockID]");
				return true;
		}
	}
	
	private boolean infoOf(CommandSender sender, String[] args) {
		// If player, check permissions
		if(sender instanceof Player && !((Player)sender).hasPermission("controlproduction.infoof")) {
			sender.sendMessage(ControlCore.getMessage(Message.COMMAND_NO_PERMISSION));
			return true;
		}
		
		// Check args
		if(args.length != 3) {
			sender.sendMessage(ControlCore.getMessage(Message.COMMAND_INCORRECT_STRUCTURE));
			sender.sendMessage("/controlproduction infoof player action");
			return true;
		}
		
		// Check target player
		OfflinePlayer target;
		try {
			target = ControlCore.getInstance().getServer().getOfflinePlayer(args[1]);
		} catch (NullPointerException ex) {
			sender.sendMessage(ControlCore.getMessage(Message.INFOOF_PLAYER_NOT_FOUND).replace("[PLAYER]", args[1]));
			return true;
		}
		
		// Check target is in group
		PlayerData data = new PlayerData((Player)target);
		if(!data.isInGroup()) {
			sender.sendMessage(ControlCore.getMessage(Message.INFOOF_NOT_IN_GROUP).replace("[PLAYER]", args[1]));
			return true;
		}
		
		// Check valid action
		ControlAction action;
		try {
			action = Enum.valueOf(ControlAction.class, args[2].toUpperCase());
		} catch (NullPointerException ex) {
			sender.sendMessage(ControlCore.getMessage(Message.COMMAND_INVALID_ACTION));
			return true;
		}
		
		// Check target has limits for action
		if(!PlayerData.limitExists(action, null, data.getGroup())) {
			sender.sendMessage(ControlCore.getMessage(Message.INFOOF_NO_LIMIT).replace("[PLAYER]", args[1]));
			return true;
		}
		
		// Show limits
		Object[] limitsForAction = ControlCore.getInstance()
												.getConfig()
												.getConfigurationSection("groups")
												.getConfigurationSection(data.getGroup())
												.getConfigurationSection(action.toString())
												.getKeys(false)
												.toArray();
		
		sender.sendMessage(ChatColor.YELLOW + ControlCore.getMessage(Message.INFO_HEADER)
				.replace("[ACTION]", action.toString().toLowerCase()));
		
		for(Object limitKey : limitsForAction) {
			Material block  = Material.getMaterial(Integer.parseInt((String)limitKey));
			int limitActual = data.getLimit(action, block);
			int playerLimit = limitActual - data.getDone(action, block);
			
			sender.sendMessage(ChatColor.YELLOW + ControlCore.getMessage(Message.INFOOF_LIST)
					.replace("[PLAYER]", args[1])
					.replace("[ACTION]", action.toString().toLowerCase())
					.replace("[LIMIT]", playerLimit + "")
					.replace("[ITEM]", block.toString()));
		}
		
		return true;
	}

}
