package au.com.gharib.jared.lummox.controlproduction;

public enum Message {
	LIMIT_REACHED(0),
	
	COMMAND_NO_PERMISSION(1),
	COMMAND_INCORRECT_STRUCTURE(2),
	COMMAND_INVALID_ACTION(3),
	COMMAND_NUMBER_FORMAT_EXCEPTION(4),
	COMMAND_PLAYERS_ONLY(5),
	
	LIMITS_RESET_PLAYER(6),
	LIMITS_RESET_ALL(7),
	
	INFO_LIST(8),
	INFO_NOT_IN_GROUP(9),
	INFO_NO_LIMIT(10),
	INFO_HEADER(11),
	
	INFOOF_PLAYER_NOT_FOUND(12),
	INFOOF_HEADER(13),
	INFOOF_NOT_IN_GROUP(14),
	INFOOF_NO_LIMIT(15),
	INFOOF_LIST(16);
	
	

	private int i;
	
	Message(int i) {
		this.i = i;
	}
	
	public int getIndex() {
		return i;
	}
}
